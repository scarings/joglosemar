package infomedia.joglosemar.Activity;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import java.util.ArrayList;
import java.util.List;

import infomedia.joglosemar.Fragment.FourStarFragment;
import infomedia.joglosemar.Fragment.Nearby;
import infomedia.joglosemar.Fragment.OneStarFragment;
import infomedia.joglosemar.R;
import infomedia.joglosemar.Fragment.ThreeFragment;
import infomedia.joglosemar.Fragment.TwoStarFragment;

public class AtrractionActivity extends AppCompatActivity {

    private Toolbar toolbar;
    //private TrackerService trackerService;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private String PROJECT_NUMBER;
    private Menu menu;
    public static FloatingActionButton fab;
    ViewPagerAdapter adapter;
    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        coordinatorLayout=(CoordinatorLayout)findViewById(R.id.coodinator);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(null);
        setSupportActionBar(toolbar);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        /*Intent intent=new Intent(MainActivity.this, TrackerService.class);
        startService(intent);*/
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        //Log.d("RegId", "RegId: " + pushClientManager.getRegistrationId(this));
    }



    private void setupViewPager(ViewPager viewPager) {
        adapter= new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Nearby(), "Nearby");
        adapter.addFragment(new OneStarFragment(), "Adventure");
        adapter.addFragment(new TwoStarFragment(), "Beach");
        adapter.addFragment(new ThreeFragment(), "Culture");
        adapter.addFragment(new FourStarFragment(), "Other");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }



        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {

            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }



        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            switch (position)
            {

            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

}
