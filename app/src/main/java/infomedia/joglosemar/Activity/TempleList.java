package infomedia.joglosemar.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.afollestad.bridge.Bridge;
import com.afollestad.bridge.BridgeException;
import com.afollestad.bridge.Response;
import com.afollestad.bridge.ResponseConvertCallback;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import infomedia.joglosemar.Adapter.TempleListAdapter;
import infomedia.joglosemar.Helper.BridgeErrorHelper;
import infomedia.joglosemar.Helper.ConvertStringJsonHelper;
import infomedia.joglosemar.Model.ApiUrl;
import infomedia.joglosemar.Model.TempleModel;
import infomedia.joglosemar.Model.TemplePriceGroupModel;
import infomedia.joglosemar.R;

public class TempleList extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;

    private ArrayList<TempleModel> templeModelArrayList;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temple_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        init();
        progressDialog.show();
        getData();

    }

    private void init()
    {
        recyclerView=(RecyclerView)findViewById(R.id.recycler);
        templeModelArrayList=new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(TempleList.this);
        recyclerView.setLayoutManager(layoutManager);
        progressDialog=new ProgressDialog(TempleList.this);
        progressDialog.setMessage("Loading...");

    }

    private void getData()
    {
        Bridge.get(ApiUrl.TEMPLE_LIST)
                .asString(new ResponseConvertCallback<String>() {
                    @Override
                    public void onResponse(@NonNull Response response, @Nullable String s, @Nullable BridgeException e) {
                        progressDialog.dismiss();
                        if(e!=null)
                        {
                            Toast.makeText(TempleList.this, BridgeErrorHelper.findErrorBridge(e.reason(), TempleList.this), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }else {
                            System.out.println(s);
                            JsonObject jsonObject= ConvertStringJsonHelper.convertStringJson(s);
                            if(jsonObject!=null)
                            {
                                if(jsonObject.get("status").getAsString().equalsIgnoreCase("success"))
                                {
                                    JsonArray jsonArray=(JsonArray)jsonObject.get("data");
                                    if(jsonArray.size()>0)
                                    {
                                        templeModelArrayList=new ArrayList<TempleModel>();
                                        for (int i = 0; i <jsonArray.size() ; i++) {
                                            TempleModel templemodel=new TempleModel();
                                            JsonObject templeObject=(JsonObject)jsonArray.get(i);
                                            templemodel.setId(templeObject.get("id").getAsString());
                                            if(templeObject.get("image").isJsonNull())
                                            {
                                                templemodel.setImage("fhgfg");

                                            }else {
                                                templemodel.setImage(templeObject.get("image").getAsString());
                                            }

                                            templemodel.setNama(templeObject.get("nama").getAsString());
                                            templemodel.setType(templeObject.get("type").getAsString());
                                            JsonArray arrayPrice=(JsonArray)templeObject.get("pricegroup");
                                            ArrayList<TemplePriceGroupModel> templePriceGroupModelArrayList=new ArrayList<TemplePriceGroupModel>();
                                            for (int j = 0; j <arrayPrice.size() ; j++) {
                                                TemplePriceGroupModel templePriceGroupModel=new TemplePriceGroupModel();
                                                JsonObject priceObject=(JsonObject)arrayPrice.get(j);
                                                templePriceGroupModel.setCurrency(priceObject.get("currency").getAsString());
                                                templePriceGroupModel.setName(priceObject.get("name").getAsString());
                                                templePriceGroupModel.setPrice(priceObject.get("price").getAsString());
                                                templePriceGroupModel.setPriceGroup(priceObject.get("price_group").getAsString());
                                                templePriceGroupModel.setWisataId(priceObject.get("wisata_id").getAsString());
                                                templePriceGroupModel.setTotalTicket(0);
                                                templePriceGroupModelArrayList.add(templePriceGroupModel);
                                            }
                                            templemodel.setTemplePriceGroupModelArrayList(templePriceGroupModelArrayList);
                                            templeModelArrayList.add(templemodel);
                                        }
                                        RecyclerView.Adapter mAdapter=new TempleListAdapter(templeModelArrayList, TempleList.this);
                                        recyclerView.setAdapter(mAdapter);

                                    }
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
