package infomedia.joglosemar.Activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.afollestad.bridge.Bridge;
import com.afollestad.bridge.BridgeException;
import com.afollestad.bridge.Form;
import com.afollestad.bridge.Response;
import com.afollestad.bridge.ResponseConvertCallback;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import infomedia.joglosemar.Adapter.POIListAdapter;
import infomedia.joglosemar.Helper.BridgeErrorHelper;
import infomedia.joglosemar.Helper.ConvertStringJsonHelper;
import infomedia.joglosemar.Model.ApiUrl;
import infomedia.joglosemar.Model.POIModel;
import infomedia.joglosemar.R;

public class ListPOIActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter mAdapter;
    ArrayList<POIModel> poiModelArrayList;
    String idCat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_poi);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        recyclerView=(RecyclerView)findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(ListPOIActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        idCat=getIntent().getExtras().getString("idcat");
    }

    private void getData()
    {
        Form form=new Form();
        form.add("cat_id", idCat);
        Bridge.post(ApiUrl.LIST_POI)
                .body(form)
                .asString(new ResponseConvertCallback<String>() {
                    @Override
                    public void onResponse(@NonNull Response response, @Nullable String s, @Nullable BridgeException e) {
                        if(e!=null)
                        {
                            e.printStackTrace();
                            Toast.makeText(ListPOIActivity.this, BridgeErrorHelper.findErrorBridge(e.reason(), ListPOIActivity.this), Toast.LENGTH_LONG).show();
                        }else
                        {
                            JsonObject jsonObject= ConvertStringJsonHelper.convertStringJson(s);
                            if(jsonObject!=null)
                            {
                                if(jsonObject.get("status").getAsString().equalsIgnoreCase("success"))
                                {
                                    JsonArray jsonArray=jsonObject.getAsJsonArray("data");
                                    poiModelArrayList=new ArrayList<POIModel>();
                                    for (int i = 0; i <jsonArray.size() ; i++) {
                                        POIModel poiModel=new POIModel();
                                        JsonObject objectPOI=(JsonObject)jsonArray.get(i);
                                        poiModel.setId(objectPOI.get("id").getAsString());
                                        poiModel.setCat_id(objectPOI.get("cat_id").getAsString());
                                        poiModel.setCat_name(objectPOI.get("cat_name").getAsString());
                                        poiModel.setCat_parent(objectPOI.get("cat_parent").getAsString());
                                        poiModel.setCity_id(objectPOI.get("city_id").getAsString());
                                        poiModel.setCity_name(objectPOI.get("city_name").getAsString());
                                        poiModel.setIs_popular(objectPOI.get("is_popular").getAsString());
                                        poiModel.setIs_visible_mobile(objectPOI.get("is_visible_mobile").getAsString());
                                        poiModel.setMdm_id(objectPOI.get("mdm_id").getAsString());
                                        poiModel.setPicture(objectPOI.get("picture").getAsString());
                                        poiModel.setPoi_id(objectPOI.get("poi_id").getAsString());
                                        poiModel.setPoi_lat(objectPOI.get("poi_lat").getAsString());
                                        poiModel.setPoi_lon(objectPOI.get("poi_lon").getAsString());
                                        poiModel.setPoi_name(objectPOI.get("poi_name").getAsString());
                                        poiModel.setPrice(objectPOI.get("price").getAsString());
                                        poiModel.setRating(objectPOI.get("rating").getAsString());
                                        poiModel.setRdc_id(objectPOI.get("rdc_id").getAsString());
                                        poiModelArrayList.add(poiModel);
                                    }
                                    mAdapter=new POIListAdapter(poiModelArrayList, ListPOIActivity.this);
                                    recyclerView.setAdapter(mAdapter);
                                }

                            }
                        }
                    }
                });
    }

}
