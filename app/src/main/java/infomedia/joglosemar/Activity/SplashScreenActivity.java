package infomedia.joglosemar.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import infomedia.joglosemar.R;

public class SplashScreenActivity extends AppCompatActivity {

    private ImageView imageView3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        imageView3 = (ImageView) findViewById(R.id.imageView3);
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreenActivity.this, Navigation.class));
            }
        }, 3000);
    }

}
