package infomedia.joglosemar.Activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.bridge.Bridge;
import com.afollestad.bridge.BridgeException;
import com.afollestad.bridge.Form;
import com.afollestad.bridge.Response;
import com.afollestad.bridge.ResponseConvertCallback;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.math.BigDecimal;

import infomedia.joglosemar.Adapter.AddTicketAdapter;
import infomedia.joglosemar.Helper.BridgeErrorHelper;
import infomedia.joglosemar.Helper.ConvertStringJsonHelper;
import infomedia.joglosemar.Helper.SessionManager;
import infomedia.joglosemar.Model.ApiUrl;
import infomedia.joglosemar.Model.Constant;
import infomedia.joglosemar.Model.TempleModel;
import infomedia.joglosemar.R;

public class Detail extends AppCompatActivity {


    private android.widget.ImageView backdrop;
    private Toolbar mtoolbar;
    private android.support.design.widget.CollapsingToolbarLayout collapsing;
    private android.support.design.widget.AppBarLayout appbar;
    private android.widget.TextView tittle;
    private android.widget.TextView date;
    private android.widget.TextView desription;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private TextView totalPrice;
    private Bundle bundle;
    private LinearLayout linearCheckout;
    private TempleModel templeModel;
    private SessionManager sessionManager;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.mtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
        setEvent();
        setData();


    }

    private void init()
    {
        this.desription = (TextView) findViewById(R.id.desription);
        this.date = (TextView) findViewById(R.id.date);
        this.tittle = (TextView) findViewById(R.id.tittle);
        this.appbar = (AppBarLayout) findViewById(R.id.appbar);
        this.collapsing = (CollapsingToolbarLayout) findViewById(R.id.collapsing);
        this.mtoolbar = (Toolbar) findViewById(R.id.mtoolbar);
        this.backdrop = (ImageView) findViewById(R.id.backdrop);
        this.totalPrice=(TextView)findViewById(R.id.totalPrice);
        linearCheckout=(LinearLayout)findViewById(R.id.linearCheckout);
        recyclerView=(RecyclerView)findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(Detail.this);
        recyclerView.setLayoutManager(layoutManager);
        sessionManager=new SessionManager(Detail.this);
        bundle=getIntent().getExtras();
        progressDialog=new ProgressDialog(Detail.this);

    }

    private void setEvent()
    {
        linearCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent=new Intent(Detail.this, PaymentActivity.class);
                String price=String.valueOf(countAllTicket());
                BigDecimal bigdecimal=new BigDecimal(price);
                price=bigdecimal.stripTrailingZeros().toPlainString();
                intent.putExtra("totalPrice", price);
                intent.putExtra("placeName", templeModel.getNama());
                intent.putExtra("currency", getSelectedCurrency());
                startActivity(intent);*/
                progressDialog.setMessage("Loading..");
                progressDialog.show();
                payOrder();
            }
        });
    }

    private String currencySelected()
    {
        String currency="";
        for (int i = 0; i <templeModel.getTemplePriceGroupModelArrayList().size(); i++) {

            if(templeModel.getTemplePriceGroupModelArrayList().get(i).getTotalTicket()>0)
            {
                currency=templeModel.getTemplePriceGroupModelArrayList().get(i).getCurrency();
            }


        }
        return currency;
    }

    private void payOrder()
    {
        String price=String.valueOf(countAllTicket());
        BigDecimal bigdecimal=new BigDecimal(price);
        price=bigdecimal.stripTrailingZeros().toPlainString();
        String timeStamp= Constant.getCurrentTimeStamp().replace(".", "");
        String invoiceLocal="TWC"+templeModel.getNama()+timeStamp;
        Form form=new Form();
        form.add("invoice", invoiceLocal);
        form.add("amount", price);
        form.add("userid", sessionManager.getIdLogin());
        form.add("wisata_id", templeModel.getTemplePriceGroupModelArrayList().get(0).getWisataId());
        form.add("d1", getTicketCountByCat("D1"));
        form.add("d2", getTicketCountByCat("D2"));
        form.add("f1", getTicketCountByCat("F1"));
        form.add("f2", getTicketCountByCat("F2"));
        form.add("currency", currencySelected());
        Bridge.post(ApiUrl.PAYMENT)
                .body(form)
                .asString(new ResponseConvertCallback<String>() {
                    @Override
                    public void onResponse(@NonNull Response response, @Nullable String s, @Nullable BridgeException e) {
                        progressDialog.dismiss();
                        if(e!=null)
                        {
                            Toast.makeText(Detail.this, BridgeErrorHelper.findErrorBridge(e.reason(), Detail.this), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }else
                        {
                            JsonObject jsonObject= ConvertStringJsonHelper.convertStringJson(s);
                            if(jsonObject!=null)
                            {
                                if(jsonObject.get("status").getAsString().equalsIgnoreCase("success"))
                                {
                                    JsonObject objectData=jsonObject.getAsJsonObject("data");
                                    Intent intent=new Intent(Detail.this, PaymentActivity.class);
                                    intent.putExtra("payment_code", objectData.get("payment_code").getAsString());
                                    finish();
                                    startActivity(intent);
                                }
                            }
                        }
                    }
                });
    }

    private String getTicketCountByCat(String category)
    {

        String result="";
        outerloop:
        for (int i = 0; i <templeModel.getTemplePriceGroupModelArrayList().size() ; i++) {
            if(templeModel.getTemplePriceGroupModelArrayList().get(i).getPriceGroup().equalsIgnoreCase(category))
            {
                result=String.valueOf(templeModel.getTemplePriceGroupModelArrayList().get(i).getTotalTicket());
                break outerloop;
            }

        }
        return result;
    }

    private String getSelectedCurrency()
    {
        String currency="";
        for(int i=0; i<templeModel.getTemplePriceGroupModelArrayList().size(); i++)
        {
            if(templeModel.getTemplePriceGroupModelArrayList().get(i).getTotalTicket()>0)
            {
                currency=templeModel.getTemplePriceGroupModelArrayList().get(i).getCurrency();
            }
        }
        return currency;
    }

    private void setData()
    {
        if(bundle!=null)
        {
            templeModel=(TempleModel)bundle.getSerializable("data");
            mAdapter=new AddTicketAdapter(templeModel.getTemplePriceGroupModelArrayList(), Detail.this, sumCallBack);
            recyclerView.setAdapter(mAdapter);
            getSupportActionBar().setTitle(templeModel.getNama());
            tittle.setText(templeModel.getNama());
            Picasso.with(Detail.this)
                    .load(ApiUrl.IMAGE_URL+templeModel.getImage())
                    .placeholder(R.drawable.hotelroom)
                    .into(backdrop);
        }
    }

    AddTicketAdapter.SumCallBack sumCallBack=new AddTicketAdapter.SumCallBack() {
        @Override
        public void refreshData(int adapterPosition, int sumTicket, boolean isPlus) {
            templeModel.getTemplePriceGroupModelArrayList().get(adapterPosition).setTotalTicket(sumTicket);
            if(countAllTicket()==0 || countAllTicket()==0.0)
            {
                linearCheckout.setVisibility(View.GONE);
            }else
            {
                linearCheckout.setVisibility(View.VISIBLE);
                totalPrice.setText(String.valueOf(countAllTicket()));
            }
        }
    };

    public float countAllTicket()
    {
        float total=0;
        for(int i=0; i<templeModel.getTemplePriceGroupModelArrayList().size(); i++)
        {
            total=total+(Integer.parseInt(templeModel.getTemplePriceGroupModelArrayList().get(i).getPrice())*templeModel.getTemplePriceGroupModelArrayList().get(i).getTotalTicket());
        }
        return total;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                if(countAllTicket()==0.0 || countAllTicket()==0)
                {
                    super.onBackPressed();
                }else
                {

                    AlertDialog.Builder mBuilder=new AlertDialog.Builder(Detail.this);
                    mBuilder.setMessage("Apakah Anda yakin membatalkan pesanan Anda?");
                    mBuilder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    mBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                            Detail.this.finish();
                        }
                    });
                    mBuilder.create().show();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(countAllTicket()==0.0 || countAllTicket()==0)
        {
            super.onBackPressed();
        }else
        {

            AlertDialog.Builder mBuilder=new AlertDialog.Builder(Detail.this);
            mBuilder.setMessage("Apakah Anda yakin membatalkan pesanan Anda?");
            mBuilder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            mBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                    Detail.this.finish();
                }
            });
            mBuilder.create().show();
        }

    }
}
