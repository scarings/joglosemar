package infomedia.joglosemar.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;

import infomedia.joglosemar.Helper.SessionManager;
import infomedia.joglosemar.R;

public class Navigation extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private android.widget.ImageView imageView2;
    private android.widget.TextView textView3;
    private android.widget.TextView textView4;
    private android.widget.TextView textView2;
    private android.widget.TextClock textClock;
    private android.widget.ImageButton imageButton;
    private android.widget.TextView textView5;
    private android.widget.ImageButton imageButton2;
    private android.widget.TextView textView6;
    private android.widget.ImageButton imageButton6;
    private android.widget.TextView textView10;
    private android.widget.ImageButton imageButton3;
    private android.widget.TextView textView7;
    private android.widget.ImageButton imageButton4;
    private android.widget.TextView textView8;
    private android.widget.ScrollView scrollView;
    private ImageButton hotelButton;
    private ImageButton restaurantButton;
    private ImageButton oleolehButton;
    private ImageButton transportasiButton;
    private ImageButton attractionButton;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        this.scrollView = (ScrollView) findViewById(R.id.scrollView);
        this.attractionButton = (ImageButton) findViewById(R.id.attractionButton);
        this.transportasiButton = (ImageButton) findViewById(R.id.transportasiButton);
        this.oleolehButton = (ImageButton) findViewById(R.id.oleolehButton);
        this.restaurantButton = (ImageButton) findViewById(R.id.restaurantButton);
        this.hotelButton = (ImageButton) findViewById(R.id.hotelButton);

        this.imageView2 = (ImageView) findViewById(R.id.imageView2);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        hotelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(Navigation.this, HotelActivity.class));
            }
        });
        restaurantButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(Navigation.this, RestaurantActivity.class));
            }
        });

        oleolehButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(Navigation.this, OleholehActivity.class));
            }
        });

        attractionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Navigation.this, TempleList.class));
            }
        });

        transportasiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(Navigation.this, TransportActivity.class));
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_account)
        {
            startActivity(new Intent(Navigation.this, HistoryActivity.class));
        }else if (id == R.id.nav_logout)
        {
            finish();
            SessionManager sessionManager=new SessionManager(Navigation.this);
            sessionManager.logOut();
            startActivity(new Intent(Navigation.this, LoginActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
