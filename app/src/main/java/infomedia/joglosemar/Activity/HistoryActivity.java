package infomedia.joglosemar.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.afollestad.bridge.Bridge;
import com.afollestad.bridge.BridgeException;
import com.afollestad.bridge.Response;
import com.afollestad.bridge.ResponseConvertCallback;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import infomedia.joglosemar.Adapter.HistoryPaymentAdapter;
import infomedia.joglosemar.Helper.BridgeErrorHelper;
import infomedia.joglosemar.Helper.ConvertStringJsonHelper;
import infomedia.joglosemar.Helper.SessionManager;
import infomedia.joglosemar.Model.HistoryPaymentModel;
import infomedia.joglosemar.R;

public class HistoryActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private SessionManager sessionManager;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        recyclerView=(RecyclerView)findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        sessionManager=new SessionManager(HistoryActivity.this);
        layoutManager=new LinearLayoutManager(HistoryActivity.this);
        progressDialog=new ProgressDialog(HistoryActivity.this);
        progressDialog.setMessage("Loading data...");
        recyclerView.setLayoutManager(layoutManager);
        getDataTranction();
    }

    private void getDataTranction()
    {
        progressDialog.show();
        Bridge.get("http://103.252.163.82:8008/api_joglo/index.php/apicontroller/transList/userid/"+sessionManager.getIdLogin())
                .asString(new ResponseConvertCallback<String>() {
                    @Override
                    public void onResponse(@NonNull Response response, @Nullable String s, @Nullable BridgeException e) {
                        progressDialog.dismiss();
                        if(e!=null)
                        {
                            e.printStackTrace();
                            Toast.makeText(HistoryActivity.this, BridgeErrorHelper.findErrorBridge(e.reason(), HistoryActivity.this), Toast.LENGTH_SHORT).show();
                        }else
                        {
                            JsonObject jsonObject= ConvertStringJsonHelper.convertStringJson(s);
                            ArrayList<HistoryPaymentModel> historyPaymentModelArrayList=new ArrayList<HistoryPaymentModel>();
                            if(jsonObject!=null)
                            {
                                if(jsonObject.get("status").getAsString().equalsIgnoreCase("success"))
                                {
                                    JsonArray jsonArray=jsonObject.getAsJsonArray("data");
                                    for (int i = 0; i < jsonArray.size(); i++) {
                                        JsonObject jsonData=jsonArray.get(i).getAsJsonObject();
                                        HistoryPaymentModel historyPaymentModel=new HistoryPaymentModel();
                                        historyPaymentModel.setAmount(jsonData.get("amount").getAsString());
                                        historyPaymentModel.setInvoice(jsonData.get("invoice").getAsString());
                                        historyPaymentModel.setPaymentCode(jsonData.get("payment_code").getAsString());
                                        switch (jsonData.get("result_code").getAsString())
                                        {
                                            case "05":
                                                historyPaymentModel.setResultDesc("Payment Expired");
                                                break;
                                            case "00":
                                                historyPaymentModel.setResultDesc("Payment Success");
                                                break;
                                            default:
                                                historyPaymentModel.setResultDesc("Payment Pending");
                                                break;
                                        }
                                        historyPaymentModelArrayList.add(historyPaymentModel);

                                    }

                                }
                            }
                            mAdapter=new HistoryPaymentAdapter(historyPaymentModelArrayList, HistoryActivity.this);
                            recyclerView.setAdapter(mAdapter);
                        }
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
