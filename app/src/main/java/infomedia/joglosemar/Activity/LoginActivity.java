package infomedia.joglosemar.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.bridge.Bridge;
import com.afollestad.bridge.BridgeException;
import com.afollestad.bridge.Form;
import com.afollestad.bridge.Response;
import com.afollestad.bridge.ResponseConvertCallback;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import infomedia.joglosemar.Helper.BridgeErrorHelper;
import infomedia.joglosemar.Helper.ConvertStringJsonHelper;
import infomedia.joglosemar.Helper.SessionManager;
import infomedia.joglosemar.Model.ApiUrl;
import infomedia.joglosemar.R;

public class LoginActivity extends AppCompatActivity {

    private android.widget.ImageView imageView4;
    private android.widget.EditText editText;
    private android.widget.EditText editText2;
    private android.widget.Button button;
    private Button registerButton;
    private SessionManager sessionManager;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_login);
        this.button = (Button) findViewById(R.id.button);
        registerButton=(Button)findViewById(R.id.buttonRegister);
        this.editText2 = (EditText) findViewById(R.id.editText2);
        this.editText = (EditText) findViewById(R.id.editText);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editText.getText().toString().trim().length()==0)
                {
                    Toast.makeText(LoginActivity.this, "Anda Harus mengisi email", Toast.LENGTH_LONG).show();
                }else if(editText2.getText().toString().trim().length()==0)
                {
                    Toast.makeText(LoginActivity.this, "Anda Harus mengisi password", Toast.LENGTH_LONG).show();
                }else
                {
                    progressDialog.setMessage("Loading ...");
                    progressDialog.show();
                    postLogin();
                }
            }
        });
        progressDialog=new ProgressDialog(LoginActivity.this);
        sessionManager=new SessionManager(LoginActivity.this);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });
        if(sessionManager.getSessionLogin())
        {
            finish();
            startActivity(new Intent(LoginActivity.this, Navigation.class ));
        }


    }

    private void postLogin()
    {
        Form form=new Form();
        form.add("email", editText.getText().toString());
        form.add("password", editText2.getText().toString());
        Bridge.post(ApiUrl.SIGNIN)
                .body(form)
                .asString(new ResponseConvertCallback<String>() {
                    @Override
                    public void onResponse(@NonNull Response response, @Nullable String s, @Nullable BridgeException e) {
                        progressDialog.dismiss();
                        if(e!=null)
                        {
                            e.printStackTrace();
                            Toast.makeText(LoginActivity.this, BridgeErrorHelper.findErrorBridge(e.reason(), LoginActivity.this), Toast.LENGTH_LONG).show();
                        }else
                        {
                            System.out.println(s);
                            JsonObject jsonObject= ConvertStringJsonHelper.convertStringJson(s);
                            if(jsonObject!=null)
                            {
                                if(jsonObject.get("status").getAsString().equalsIgnoreCase("success"))
                                {

                                    sessionManager.createSessionLogin();
                                    JsonArray jsonArray=(JsonArray)jsonObject.get("data");
                                    for(int i=0; i<jsonArray.size(); i++)
                                    {
                                        JsonObject loginObject=(JsonObject)jsonArray.get(i);
                                        sessionManager.saveDataLogin(loginObject.get("id").getAsString(),
                                                loginObject.get("nama").getAsString(),
                                                loginObject.get("email").getAsString(),
                                                loginObject.get("noktp").getAsString());
                                    }
                                    finish();
                                    startActivity(new Intent(LoginActivity.this, Navigation.class));
                                }else
                                {
                                    Toast.makeText(LoginActivity.this, "Username atau password salah", Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    }
                });
    }

}
