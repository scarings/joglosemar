package infomedia.joglosemar.Activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.bridge.Bridge;
import com.afollestad.bridge.BridgeException;
import com.afollestad.bridge.Form;
import com.afollestad.bridge.Response;
import com.afollestad.bridge.ResponseConvertCallback;
import com.google.gson.JsonObject;

import infomedia.joglosemar.Helper.BridgeErrorHelper;
import infomedia.joglosemar.Helper.ConvertStringJsonHelper;
import infomedia.joglosemar.Model.ApiUrl;
import infomedia.joglosemar.R;

public class RegisterActivity extends AppCompatActivity {

    private android.widget.EditText namaEDReg;
    private android.widget.EditText emailEDReg;
    private android.widget.EditText ktpEDReg;
    private android.widget.EditText passwordEDReg;
    private android.widget.EditText rePassEDReg;
    private android.widget.Button registerButton;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        init();
        setEvent();

    }

    private void setEvent()
    {
        this.registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkInput())
                {
                    progressDialog.show();
                    postRegister();
                }
            }
        });
    }

    private void postRegister()
    {
        Form form=new Form();
        form.add("email", emailEDReg.getText().toString());
        form.add("nama", namaEDReg.getText().toString());
        form.add("password", passwordEDReg.getText().toString());
        form.add("ktp", ktpEDReg.getText().toString());
        Bridge.post(ApiUrl.REGISTER)
                .body(form)
                .asString(new ResponseConvertCallback<String>() {
                    @Override
                    public void onResponse(@NonNull Response response, @Nullable String s, @Nullable BridgeException e) {
                        progressDialog.dismiss();
                        if(e!=null)
                        {
                            Toast.makeText(RegisterActivity.this, BridgeErrorHelper.findErrorBridge(e.reason(), RegisterActivity.this), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }else
                        {
                            JsonObject jsonObject= ConvertStringJsonHelper.convertStringJson(s);
                            if(jsonObject!=null)
                            {
                                if(jsonObject.get("status").getAsString().equalsIgnoreCase("success"))
                                {
                                    AlertDialog.Builder mBuilder=new AlertDialog.Builder(RegisterActivity.this);
                                    mBuilder.setMessage("Registrasi sukses");
                                    mBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                                        }
                                    });
                                    mBuilder.create().show();

                                }else
                                {
                                    Toast.makeText(RegisterActivity.this, "Terjadi kesalahan silakan ulangi lagi", Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    }
                });
    }


    private void init()
    {
        this.registerButton = (Button) findViewById(R.id.registerButton);
        this.rePassEDReg = (EditText) findViewById(R.id.rePassEDReg);
        this.passwordEDReg = (EditText) findViewById(R.id.passwordEDReg);
        this.ktpEDReg = (EditText) findViewById(R.id.ktpEDReg);
        this.emailEDReg = (EditText) findViewById(R.id.emailEDReg);
        this.namaEDReg = (EditText) findViewById(R.id.namaEDReg);
        progressDialog=new ProgressDialog(RegisterActivity.this);
        progressDialog.setMessage("Loading data...");
    }

    private boolean checkInput()
    {
        if(ktpEDReg.getText().toString().trim().length()==0)
        {
            Toast.makeText(RegisterActivity.this, "Anda harus mengisi nomor KTP", Toast.LENGTH_SHORT).show();
            return false;
        }else if(namaEDReg.getText().toString().trim().length()==0)
        {
            Toast.makeText(RegisterActivity.this, "Anda harus mengisi nama", Toast.LENGTH_SHORT).show();
            return false;
        }else if(emailEDReg.getText().toString().trim().length()==0)
        {
            Toast.makeText(RegisterActivity.this, "Anda harus mengisi email", Toast.LENGTH_SHORT).show();
            return false;
        }else if(passwordEDReg.getText().toString().trim().length()==0)
        {
            Toast.makeText(RegisterActivity.this, "Anda harus mengisi password", Toast.LENGTH_SHORT).show();
            return false;
        }else if(rePassEDReg.getText().toString().trim().length()==0)
        {
            Toast.makeText(RegisterActivity.this, "Anda harus mengisi konfirmasi password", Toast.LENGTH_SHORT).show();
            return false;
        }else if(!passwordEDReg.getText().toString().equalsIgnoreCase(rePassEDReg.getText().toString()))
        {
            Toast.makeText(RegisterActivity.this, "Password dan konfirmasi password harus sama", Toast.LENGTH_SHORT).show();
            return false;
        }else
            return true;

    }

}
