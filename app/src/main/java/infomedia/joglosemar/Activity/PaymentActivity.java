package infomedia.joglosemar.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import infomedia.joglosemar.R;

public class PaymentActivity extends AppCompatActivity {

    private WebView webView;
    private Bundle bundle;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        webView=(WebView)findViewById(R.id.webview);
        bundle=getIntent().getExtras();
        progressDialog=new ProgressDialog(PaymentActivity.this);
        String url="http://103.252.163.82:8008/api_joglo/index.php/faq/index/"+bundle.getString("payment_code");
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.setWebChromeClient(new WebChromeClient(){
            public void onProgressChanged(WebView view, int progress)
            {
                if(progress<80)
                {
                    progressDialog.show();
                }else
                {
                    progressDialog.dismiss();
                }
            }

        });
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                return false;
            }
        });
        webView.loadUrl(url);
        CookieManager.getInstance().acceptCookie();


    }

    public String getTime()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        //get current date time with Date()
        Date date = new Date();
        dateFormat.format(date);
        return dateFormat.format(date);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.payment_info, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.action_close:
                finish();
                startActivity(new Intent(PaymentActivity.this, Navigation.class));
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        startActivity(new Intent(PaymentActivity.this, Navigation.class));
    }
}
