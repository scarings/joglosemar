package infomedia.joglosemar.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import infomedia.joglosemar.Activity.Detail;
import infomedia.joglosemar.Model.POIModel;
import infomedia.joglosemar.R;

/**
 * Created by 1sf4n on 30/06/2016.
 */
public class POIListAdapter extends RecyclerView.Adapter<POIListAdapter.ViewHolder> {
    private ArrayList<POIModel> mDataset;

    Context context;


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView name;
        private TextView description;
        private RatingBar ratingBar;
        private ImageView img;


        public ViewHolder(View v) {
            super(v);
            this.img = (ImageView) v.findViewById(R.id.img);
            this.ratingBar = (RatingBar) v.findViewById(R.id.ratingBar);
            this.description = (TextView) v.findViewById(R.id.description);
            this.name = (TextView) v.findViewById(R.id.name);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context, Detail.class);
                    intent.putExtra("data", mDataset.get(getAdapterPosition()));
                    context.startActivity(intent);
                }
            });

        }
    }

    public void add(int position, POIModel model) {
        mDataset.add(position, model);
        notifyItemInserted(position);
    }

    public void remove(String item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public POIListAdapter(ArrayList<POIModel> myDataset, Context context) {
        mDataset = myDataset;
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public POIListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_list, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final POIModel name = mDataset.get(position);
        holder.name.setText(name.getPoi_name());
        holder.description.setText(name.getCity_name());
        Picasso.with(context)
                .load(name.getPicture())
                .placeholder(R.drawable.joglosemar)
                .into(holder.img);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}

