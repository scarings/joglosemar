package infomedia.joglosemar.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import infomedia.joglosemar.Model.TemplePriceGroupModel;
import infomedia.joglosemar.R;

/**
 * Created by 1sf4n on 22/06/2016.
 */
public class AddTicketAdapter extends RecyclerView.Adapter<AddTicketAdapter.ViewHolder> {
    private ArrayList<TemplePriceGroupModel> mDataset;
    SumCallBack sumCallBack;
    Context context;


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        private TextView nameTicket;
        private TextView priceTicket;
        private ImageView addTicket;
        private TextView sumTicket;
        private ImageView removeTicket;



        public ViewHolder(View v) {
            super(v);
            this.removeTicket = (ImageView) v.findViewById(R.id.removeTicket);
            this.sumTicket = (TextView) v.findViewById(R.id.sumTicket);
            this.addTicket = (ImageView) v.findViewById(R.id.addTicket);
            this.priceTicket = (TextView) v.findViewById(R.id.priceTicket);
            this.nameTicket = (TextView) v.findViewById(R.id.nameTicket);
            addTicket.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isAllowAdd(getAdapterPosition()))
                    {
                        sumTicket.setText(Integer.toString(mDataset.get(getAdapterPosition()).getTotalTicket()+1));
                        sumCallBack.refreshData(getAdapterPosition(), Integer.parseInt(sumTicket.getText().toString()), true);
                    }else
                    {
                        Toast.makeText(context, "Anda tidak bisa menambahkan tiket dengan currency "+getSelectedCurrency()+", silakan tiket dengan currency sama", Toast.LENGTH_LONG ).show();
                    }

                }
            });

            removeTicket.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(Integer.parseInt(sumTicket.getText().toString())==0)
                    {

                    }else
                    {
                        sumTicket.setText(Integer.toString(mDataset.get(getAdapterPosition()).getTotalTicket()-1));
                        sumCallBack.refreshData(getAdapterPosition(), Integer.parseInt(sumTicket.getText().toString()), false);
                    }
                }
            });

        }
    }

    private boolean isAllowAdd(int position)
    {
        boolean isAllow=true;
        for (int i = 0; i <mDataset.size() ; i++) {

            if(!mDataset.get(i).getCurrency().equalsIgnoreCase(mDataset.get(position).getCurrency()) && mDataset.get(i).getTotalTicket()>0)
            {
                isAllow=false;
                break;
            }


        }
        return isAllow;
    }

    private String getSelectedCurrency()
    {

        String currentCurreny="";
        for (int i = 0; i <mDataset.size() ; i++) {
            if(currentCurreny.equalsIgnoreCase(""))
            {
                if(mDataset.get(i).getTotalTicket()>0)
                {
                    currentCurreny=mDataset.get(i).getCurrency();
                }
            }else {
                if(mDataset.get(i).getTotalTicket()>0)
                {
                    if(!currentCurreny.equalsIgnoreCase(mDataset.get(i).getCurrency()))
                    {
                        currentCurreny=mDataset.get(i).getCurrency();
                        break;
                    }
                }
            }

        }
        return currentCurreny;
    }

    public void add(int position, TemplePriceGroupModel model) {
        mDataset.add(position, model);
        notifyItemInserted(position);
    }

    public void remove(String item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AddTicketAdapter(ArrayList<TemplePriceGroupModel> myDataset, Context context, SumCallBack sumCallBack) {
        mDataset = myDataset;
        this.context = context;
        this.sumCallBack=sumCallBack;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AddTicketAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_add_ticket, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final TemplePriceGroupModel name = mDataset.get(position);
        holder.nameTicket.setText(name.getName());
        holder.sumTicket.setText(Integer.toString(0));
        holder.priceTicket.setText(name.getCurrency()+" "+name.getPrice());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static abstract class SumCallBack{
        public abstract void refreshData(int adapterPosition, int sumTicket, boolean isplus);
    }
}