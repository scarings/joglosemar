package infomedia.joglosemar.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

import infomedia.joglosemar.Activity.Detail;
import infomedia.joglosemar.Model.HotelModel;
import infomedia.joglosemar.R;

/**
 * Created by 1sf4n on 09/03/2016.
 */
public class HotelAdapter extends RecyclerView.Adapter<HotelAdapter.ViewHolder> {
    private ArrayList<HotelModel> mDataset;

    Context context;


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private android.widget.ImageView imageView5;
        private android.widget.TextView textView9;
        private android.widget.TextView textView11;
        private android.widget.RatingBar ratingBar;

        public ViewHolder(View v) {
            super(v);
            this.ratingBar = (RatingBar) v.findViewById(R.id.ratingBar);
            this.textView11 = (TextView) v.findViewById(R.id.description);
            this.textView9 = (TextView) v.findViewById(R.id.name);
            this.imageView5 = (ImageView) v.findViewById(R.id.img);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, Detail.class));
                }
            });

        }
    }

    public void add(int position, HotelModel model) {
        mDataset.add(position, model);
        notifyItemInserted(position);
    }

    public void remove(String item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public HotelAdapter(ArrayList<HotelModel> myDataset, Context context) {
        mDataset = myDataset;
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public HotelAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_list, parent, false);

        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final HotelModel name = mDataset.get(position);
        holder.textView9.setText(name.getHotelName());
        holder.textView11.setText(name.getHotelLoc());


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
