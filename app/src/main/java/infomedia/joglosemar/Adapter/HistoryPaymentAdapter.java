package infomedia.joglosemar.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import infomedia.joglosemar.Model.HistoryPaymentModel;
import infomedia.joglosemar.R;

/**
 * Created by 1sf4n on 23/06/2016.
 */
public class HistoryPaymentAdapter extends RecyclerView.Adapter<HistoryPaymentAdapter.ViewHolder> {
    private ArrayList<HistoryPaymentModel> mDataset;

    Context context;



    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView paymentCode;
        private TextView invoice;
        private TextView totalPrice;
        private TextView statusBayar;

        public ViewHolder(View v) {
            super(v);
            this.statusBayar = (TextView) v.findViewById(R.id.statusBayar);
            this.totalPrice = (TextView) v.findViewById(R.id.totalPrice);
            this.invoice = (TextView) v.findViewById(R.id.invoice);
            this.paymentCode = (TextView) v.findViewById(R.id.paymentCode);


        }
    }

    public void add(int position, HistoryPaymentModel model) {
        mDataset.add(position, model);
        notifyItemInserted(position);
    }

    public void remove(String item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public HistoryPaymentAdapter(ArrayList<HistoryPaymentModel> myDataset, Context context) {
        mDataset = myDataset;
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public HistoryPaymentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_history_transaction, parent, false);


        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final HistoryPaymentModel name = mDataset.get(position);
        holder.invoice.setText(name.getInvoice());
        holder.paymentCode.setText(name.getPaymentCode());
        holder.statusBayar.setText(name.getResultDesc());
        holder.totalPrice.setText(name.getAmount());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
