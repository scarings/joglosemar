package infomedia.joglosemar.Helper;

import android.content.Context;

import com.afollestad.bridge.BridgeException;

import infomedia.joglosemar.R;

/**
 * Created by 1sf4n on 22/06/2016.
 */
public class BridgeErrorHelper {
    public  static String findErrorBridge(int errCode, Context context)
    {
        final int errorCancel= BridgeException.REASON_REQUEST_CANCELLED;
        final int errorTimeout=BridgeException.REASON_REQUEST_TIMEOUT;
        final int errorFailed=BridgeException.REASON_REQUEST_FAILED;
        final int errorUnsuccess=BridgeException.REASON_RESPONSE_UNSUCCESSFUL;
        final int errorUnparse=BridgeException.REASON_RESPONSE_UNPARSEABLE;
        final int errorIOError=BridgeException.REASON_RESPONSE_IOERROR;
        final int errorValidatorFalse=BridgeException.REASON_RESPONSE_VALIDATOR_FALSE;
        final int errorValidatorError=BridgeException.REASON_RESPONSE_VALIDATOR_ERROR;
        String result="";
        if(context!=null)
        {
            switch (errCode) {
                case errorCancel:
                    result=context.getString(R.string.bridgerequesterror);
                    break;
                case errorTimeout:
                    result=context.getString(R.string.bridgetimeouterror);
                    break;
                case errorFailed:
                    result=context.getString(R.string.bridgefailedconnect);
                    break;
                case errorUnsuccess:
                    result=context.getString(R.string.bridgeunsuccess);
                    break;
                case errorUnparse:
                    result=context.getString(R.string.bridgecantparse);
                    break;
                case errorIOError:
                    result=context.getString(R.string.bridgeIOerror);
                    break;
                case errorValidatorError:
                    result=context.getString(R.string.bridgeeerrorvalidator);
                    break;
                case errorValidatorFalse:
                    result=context.getString(R.string.bridgefalsevalidator);
                    break;
            }
        }
        return result;
    }
}
