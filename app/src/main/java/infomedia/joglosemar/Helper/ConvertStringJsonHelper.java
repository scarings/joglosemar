package infomedia.joglosemar.Helper;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Created by 1sf4n on 22/06/2016.
 */
public class ConvertStringJsonHelper {
    /*
    * Class untuk konversi String ke Gson JsonObject
    *
    * */

    public static final JsonObject convertStringJson(String source)
    {
        JsonObject jsonObject=null;
        try{
            JsonParser jsonParser=new JsonParser();
            jsonObject=(JsonObject)jsonParser.parse(source.trim());
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
