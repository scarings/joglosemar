package infomedia.joglosemar.Helper;

import android.content.Context;
import android.content.SharedPreferences;

import infomedia.joglosemar.Model.Constant;

/**
 * Created by 1sf4n on 22/06/2016.
 */
public class SessionManager {

    SharedPreferences.Editor editor;
    SharedPreferences sharePref;
    Context context;

    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "joglosemar";


    public SessionManager(Context context) {
        this.context = context;
        sharePref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharePref.edit();
    }

    public void createSessionLogin()
    {
        editor.putBoolean(Constant.SESSION_LOGIN, true);
        editor.commit();
    }

    public void saveDataLogin(String idLogin,String nama, String email, String noktp)
    {
        editor.putString(Constant.ID_LOGIN, idLogin);
        editor.putString(Constant.NAMA_LOGIN, nama);
        editor.putString(Constant.EMAIL_LOGIN, email);
        editor.putString(Constant.NOKTP_LOGIN, noktp);
        editor.commit();
    }

    public void logOut()
    {
        editor.clear();
        editor.commit();
    }

    public boolean getSessionLogin(){return sharePref.getBoolean(Constant.SESSION_LOGIN,false);}
    public String getNamaLogin(){return  sharePref.getString(Constant.NAMA_LOGIN, "");}
    public String getEmailLogin(){return  sharePref.getString(Constant.EMAIL_LOGIN, "");}
    public String getNoKtpLogin(){return  sharePref.getString(Constant.NOKTP_LOGIN, "");}
    public String getIdLogin(){return sharePref.getString(Constant.ID_LOGIN, "");}
}
