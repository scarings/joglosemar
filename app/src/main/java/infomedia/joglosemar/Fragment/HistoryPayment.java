package infomedia.joglosemar.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.bridge.Bridge;
import com.afollestad.bridge.BridgeException;
import com.afollestad.bridge.Response;
import com.afollestad.bridge.ResponseConvertCallback;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import infomedia.joglosemar.Adapter.HistoryPaymentAdapter;
import infomedia.joglosemar.Helper.BridgeErrorHelper;
import infomedia.joglosemar.Helper.ConvertStringJsonHelper;
import infomedia.joglosemar.Helper.SessionManager;
import infomedia.joglosemar.Model.HistoryPaymentModel;
import infomedia.joglosemar.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HistoryPayment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HistoryPayment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HistoryPayment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View view;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private OnFragmentInteractionListener mListener;
    private SessionManager sessionManager;

    public HistoryPayment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static HistoryPayment newInstance(String param1, String param2) {
        HistoryPayment fragment = new HistoryPayment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_history_payment, container, false);
        recyclerView=(RecyclerView)view.findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        sessionManager=new SessionManager(getActivity());
        layoutManager=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        getDataTranction();
        return view;
    }

    private void getDataTranction()
    {
        Bridge.get("http://103.252.163.82:8008/api_joglo/index.php/apicontroller/transList/userid/"+sessionManager.getIdLogin())
                .asString(new ResponseConvertCallback<String>() {
                    @Override
                    public void onResponse(@NonNull Response response, @Nullable String s, @Nullable BridgeException e) {
                        if(e!=null)
                        {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), BridgeErrorHelper.findErrorBridge(e.reason(), getActivity()), Toast.LENGTH_SHORT).show();
                        }else
                        {
                            JsonObject jsonObject= ConvertStringJsonHelper.convertStringJson(s);
                            ArrayList<HistoryPaymentModel> historyPaymentModelArrayList=new ArrayList<HistoryPaymentModel>();
                            if(jsonObject!=null)
                            {
                                if(jsonObject.get("status").getAsString().equalsIgnoreCase("success"))
                                {
                                    JsonArray jsonArray=jsonObject.getAsJsonArray("data");
                                    for (int i = 0; i < jsonArray.size(); i++) {
                                        JsonObject jsonData=jsonArray.get(i).getAsJsonObject();
                                        HistoryPaymentModel historyPaymentModel=new HistoryPaymentModel();
                                        historyPaymentModel.setAmount(jsonData.get("amount").getAsString());
                                        historyPaymentModel.setInvoice(jsonData.get("invoice").getAsString());
                                        historyPaymentModel.setPaymentCode(jsonData.get("payment_code").getAsString());
                                        switch (jsonData.get("result_code").getAsString())
                                        {
                                            case "05":
                                                historyPaymentModel.setResultDesc("Payment Expired");
                                                break;
                                            case "00":
                                                historyPaymentModel.setResultDesc("Payment Success");
                                                break;
                                        }
                                        historyPaymentModelArrayList.add(historyPaymentModel);

                                    }

                                }
                            }
                            mAdapter=new HistoryPaymentAdapter(historyPaymentModelArrayList, getActivity());
                            recyclerView.setAdapter(mAdapter);
                        }
                    }
                });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
