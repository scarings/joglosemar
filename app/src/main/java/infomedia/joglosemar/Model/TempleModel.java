package infomedia.joglosemar.Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by 1sf4n on 21/06/2016.
 */
public class TempleModel implements Serializable {
    private String id;
    private String nama;
    private String image;
    private String type;

    private ArrayList<TemplePriceGroupModel> templePriceGroupModelArrayList;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ArrayList<TemplePriceGroupModel> getTemplePriceGroupModelArrayList() {
        return templePriceGroupModelArrayList;
    }

    public void setTemplePriceGroupModelArrayList(ArrayList<TemplePriceGroupModel> templePriceGroupModelArrayList) {
        this.templePriceGroupModelArrayList = templePriceGroupModelArrayList;
    }
}
