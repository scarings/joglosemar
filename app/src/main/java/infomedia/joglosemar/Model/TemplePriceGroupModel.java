package infomedia.joglosemar.Model;

import java.io.Serializable;

/**
 * Created by 1sf4n on 21/06/2016.
 */
public class TemplePriceGroupModel implements Serializable{
    private String wisataId;
    private String priceGroup;
    private String currency;
    private String price;
    private String name;
    private int totalTicket;

    public int getTotalTicket() {
        return totalTicket;
    }

    public void setTotalTicket(int totalTicket) {
        this.totalTicket = totalTicket;
    }

    public String getWisataId() {
        return wisataId;
    }

    public void setWisataId(String wisataId) {
        this.wisataId = wisataId;
    }

    public String getPriceGroup() {
        return priceGroup;
    }

    public void setPriceGroup(String priceGroup) {
        this.priceGroup = priceGroup;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
