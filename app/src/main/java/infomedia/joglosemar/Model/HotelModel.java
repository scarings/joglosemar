package infomedia.joglosemar.Model;

/**
 * Created by 1sf4n on 09/03/2016.
 */
public class HotelModel {
    String hotelName;
    String hotelLoc;

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getHotelLoc() {
        return hotelLoc;
    }

    public void setHotelLoc(String hotelLoc) {
        this.hotelLoc = hotelLoc;
    }
}
