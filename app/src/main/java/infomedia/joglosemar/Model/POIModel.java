package infomedia.joglosemar.Model;

import java.io.Serializable;

/**
 * Created by 1sf4n on 30/06/2016.
 */
public class POIModel implements Serializable{
    private String id;
    private String cat_id;
    private String cat_name;
    private String cat_parent;
    private String city_id;
    private String city_name;
    private String is_popular;
    private String is_visible_mobile;
    private String mdm_id;
    private String picture;
    private String poi_id;
    private String poi_lat;
    private String poi_lon;
    private String poi_name;
    private String price;
    private String rating;
    private String rdc_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getCat_parent() {
        return cat_parent;
    }

    public void setCat_parent(String cat_parent) {
        this.cat_parent = cat_parent;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getIs_popular() {
        return is_popular;
    }

    public void setIs_popular(String is_popular) {
        this.is_popular = is_popular;
    }

    public String getIs_visible_mobile() {
        return is_visible_mobile;
    }

    public void setIs_visible_mobile(String is_visible_mobile) {
        this.is_visible_mobile = is_visible_mobile;
    }

    public String getMdm_id() {
        return mdm_id;
    }

    public void setMdm_id(String mdm_id) {
        this.mdm_id = mdm_id;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPoi_id() {
        return poi_id;
    }

    public void setPoi_id(String poi_id) {
        this.poi_id = poi_id;
    }

    public String getPoi_lat() {
        return poi_lat;
    }

    public void setPoi_lat(String poi_lat) {
        this.poi_lat = poi_lat;
    }

    public String getPoi_lon() {
        return poi_lon;
    }

    public void setPoi_lon(String poi_lon) {
        this.poi_lon = poi_lon;
    }

    public String getPoi_name() {
        return poi_name;
    }

    public void setPoi_name(String poi_name) {
        this.poi_name = poi_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getRdc_id() {
        return rdc_id;
    }

    public void setRdc_id(String rdc_id) {
        this.rdc_id = rdc_id;
    }
}
