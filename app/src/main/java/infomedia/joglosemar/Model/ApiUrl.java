package infomedia.joglosemar.Model;

/**
 * Created by 1sf4n on 22/06/2016.
 */
public class ApiUrl {
    public static final String BASE_URL="http://103.252.163.82:8008/api_joglo/index.php/apicontroller/";
    public static final String TEMPLE_LIST=BASE_URL+"listTemple";
    public static final String REGISTER=BASE_URL+"register";
    public static final String SIGNIN=BASE_URL+"signIn";
    public static final String LIST_POI=BASE_URL+"listPoi";
    public static final String IMAGE_URL="http://103.252.163.82:8008/api_joglo/file/wisata/";
    public static final String PAYMENT="http://103.252.163.82:8008/api_joglo/index.php/finpay/paymentCode";
    public static final String RETURN_URL="http://platform-lativa.infomedia.co.id:8008/finpayjoglosemar/021.return_url.php";
}
